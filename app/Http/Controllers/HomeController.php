<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class HomeController extends Controller
{
    /**

     * Show the application layout

     *

     * @return \Illuminate\Http\Response

     */

    public function myform()

    {

        $states = DB::table("demo_state")->pluck("name","id");

        return view('teste',compact('states'));

    }


    /**

     * Get Ajax Request and restun Data

     *

     * @return \Illuminate\Http\Response

     */

    public function myformAjax($id)

    {

        $cities = DB::table("demo_cities")

                    ->where("state_id",$id)

                    ->lists("name","id");

        return json_encode($cities);

    }
}
