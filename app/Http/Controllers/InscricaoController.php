<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lead;
use Redirect;
use DB; 

class InscricaoController extends Controller
{

    
    public function save(Request $req){

        $idcpf= $req->input('cpf');
        

        if (DB::table('leads')->where('cpf', $idcpf)->count('cpf') == 0) {

        $lead=new lead;
        $lead->nome = $req->nome;
        $lead->cpf = $req->cpf;
        $lead->telefone = $req->telefone;
        $lead->email = $req->email;
        $lead->unidade_id = $req->unidade;
        $lead->save();
        return view('confirmacao');         
        }

        else{
            return Redirect::to("/")->withFail('CPF já cadastrado');}
}

}

    

