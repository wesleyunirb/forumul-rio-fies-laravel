﻿<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cadastro UNIRB | Confirmação</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/redeunirb.png"/>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">


<!--===============================================================================================-->


</head>
<body>

	<div class="bg-contact100" style="background-color:#9e111f;">
		<div class="container-contact100">
			<div class="wrap-contact100">
			<p><b style="font-size:30px;color:black">Cadastro realizado com Sucesso !</b></p>
                        <p style="font-size:20px;color:black">Estamos lhe redirecionando para o site da UNIRB para continuar o cadastro.</p>
            
				<center><button class="btn btn-primary" ><a href="http://unirb.edu.br/"><span style="color:white;font-size:20px"><b>Continuar para UNIRB</b></span></a></button></center>

                
			</div>
		</div>
	</div>




<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>
