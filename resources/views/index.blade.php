﻿<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cadastro UNIRB</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/redeunirb.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    
<!-- MASK-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

	


<!--===============================================================================================-->

<script type="text/javascript">
    $("#cpf").mask("000.000.000-00");
    $("#telefone").mask("(00)0 0000-000");
    </script>
	
	@if(Session::has('fail'))
    <div class="alert alert-danger">
       {{Session::get('fail')}}
    </div>
@endif


</head>

<body>

	<div class="bg-contact100" style="background-color:#9e111f;">
		<div class="container-contact100">
			<div class="wrap-contact100">
				<div class="contact100-pic js-tilt" data-tilt>
					<img src="images/redeunirb.png" alt="IMG">
				</div>

                {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}
					<span class="contact100-form-title">
						Cadastro de candidato
					</span>




                        <div class="wrap-input100 validate-input" data-validate = "Preencha o campo">
						<input class="input100" type="text"  id="cpf"  name="cpf" placeholder="CPF" required >
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
                                              </div>


					<div class="wrap-input100 validate-input" data-validate = "Preencha o campo">
						<input class="input100" type="text"  id="nome" name="nome" placeholder="Nome completo" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
                    </div>
                    
                   
                    
                    <div class="wrap-input100 validate-input" data-validate = "Preencha o campo">
						<input class="input100" type="text"  id="telefone" name="telefone" placeholder="Telefone" required >
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Preencha o campo">
						<input class="input100" type="text" id="email" name="email" placeholder="Email" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate = "Preencha o campo">
                   <label>Selecione o local onde deseja estudar</label>                 
				    <select name="unidade" class="form-control" style="background-color:#e6e6e6">
                  @foreach ($unidades as $uni)
                 <option name="unidade" id="unidade" class="input100"  value="{{ $uni->id }}" selected="selected">{{ $uni->nome }}</option>
                 @endforeach
                 </select>
						
                    </div>


                    
                

					<div class="container-contact100-form-btn">
						<button type="submit"  class="contact100-form-btn">
							Cadastrar
						</button>
					</div>
                    {!!Form:: close()!!} 
			</div>
		</div>
	</div>






</body>
</html>
